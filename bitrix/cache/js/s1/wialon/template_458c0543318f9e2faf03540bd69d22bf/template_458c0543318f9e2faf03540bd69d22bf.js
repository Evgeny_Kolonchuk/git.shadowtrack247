
; /* Start:"a:4:{s:4:"full";s:59:"/local/templates/wialon/js/wialon_scripts.js?14828419812320";s:6:"source";s:44:"/local/templates/wialon/js/wialon_scripts.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
var dnsWialon = "https://hosting.wialon.com";
console.log(window.location.hostname);
// Main function
function getToken() {
    // construct login page URL
    var url = dnsWialon + "/login.html";
    url += "?client_id=" + window.location.hostname;
    url += "&access_type=" + 0x100;	// access level, 0x100 = "Online tracking only"
    url += "&activation_time=" + 0;	// activation time, 0 = immediately; you can pass any UNIX time value
    url += "&duration=" + 604800;	// duration, 604800 = one week in seconds
    url += "&flags=" + 0x1;			// options, 0x1 = add username in response

    url += "&redirect_uri=http://wialon.test1.newsite.by/test.php"; // if login succeed - redirect to this page

    // listen message with token from login page window
    window.addEventListener("message", tokenRecieved);

    // finally, open login page in new window
    window.open(url, "_blank", "width=760, height=500, top=300, left=500");
}

// Help function
function tokenRecieved(e) {
    // get message from login window
    var msg = e.data;

    if (typeof msg == "string" && msg.indexOf("access_token=") >= 0) {
        // get token
        var token = msg.replace("access_token=", "");
        // now we can use token, e.g show it on page
        document.getElementById("token").innerHTML = token;
        document.getElementById("login").setAttribute("disabled", "");
        document.getElementById("logout").removeAttribute("disabled");

        // or login to wialon using our token
        wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com");

        wialon.core.Session.getInstance().loginToken(token, "", function(code) {
            if (code)
                return;
            var user = wialon.core.Session.getInstance().getCurrUser().getName();
            alert("Authorized as " + user);
        });

        // remove "message" event listener
        window.removeEventListener("message", tokenRecieved);
    }
}

function logout() {
    var sess = wialon.core.Session.getInstance();
    if (sess && sess.getId()) {
        sess.logout(function() {
            document.getElementById("logout").setAttribute("disabled", "");
            document.getElementById("login").removeAttribute("disabled");
        });
    }
}
/* End */
;; /* /local/templates/wialon/js/wialon_scripts.js?14828419812320*/
