
; /* Start:"a:4:{s:4:"full";s:59:"/local/templates/wialon/js/wialon_scripts.js?14828432242943";s:6:"source";s:44:"/local/templates/wialon/js/wialon_scripts.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/

var wialonObj = new (function() {

    var dnsWialon = "https://hosting.wialon.com";
    var body = $(document.body);
    var win = $(window);
    var accessRights = '0x1';
    var duration = 604800;
    var activation_time = 0;
    var access_type = '0x100';
    var redirect_uri = 'http://wialon.test1.newsite.by/test.php';

    this.getToken = function() {

        var url = this.dnsWialon + "/login.html";
        url += "?client_id=" + window.location.hostname;
        url += "&access_type=" + access_type;
        url += "&activation_time=" + activation_time;
        url += "&duration=" + duration;
        url += "&flags=" + this.accessRights;
        url += "&redirect_uri=" + redirect_uri;

        window.addEventListener("message", this.tokenRecieved);
        window.open(url, "_blank", "width=760, height=500, top=300, left=500");
    }

    this.initParams = function( dnsWialon, accessRights, duration, activation_time, access_type, redirect_uri) {
        this.dnsWialon = dnsWialon != false ? dnsWialon : 'https://hosting.wialon.com';
        this.accessRights = accessRights != false ? accessRights : 0x1;
        this.duration = duration != false ? duration : 604800;
        this.activation_time = activation_time != false ? activation_time : 0;
        this.access_type = access_type != false ? access_type : '0x100';
        this.redirect_uri = redirect_uri != false ? redirect_uri : 'http://wialon.test1.newsite.by/test.php';
    }

    this.tokenRecieved = function (e) {
        var msg = e.data;

        if (typeof msg == "string" && msg.indexOf("access_token=") >= 0) {
            // get token
            var token = msg.replace("access_token=", "");
            // now we can use token, e.g show it on page
            document.getElementById("token").innerHTML = token;
            document.getElementById("login").setAttribute("disabled", "");
            document.getElementById("logout").removeAttribute("disabled");

            // or login to wialon using our token
            wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com");

            wialon.core.Session.getInstance().loginToken(token, "", function(code) {
                if (code)
                    return;
                var user = wialon.core.Session.getInstance().getCurrUser().getName();
                alert("Authorized as " + user);
            });

            // remove "message" event listener
            window.removeEventListener("message", tokenRecieved);
        }
    }

    this.logout = function () {
        var sess = wialon.core.Session.getInstance();
        if (sess && sess.getId()) {
            sess.logout(function() {
                document.getElementById("logout").setAttribute("disabled", "");
                document.getElementById("login").removeAttribute("disabled");
            });
        }
    }

})();
/* End */
;; /* /local/templates/wialon/js/wialon_scripts.js?14828432242943*/
