<?php
IncludeModuleLangFile(__FILE__);

/** @global string $DBType */
global $DBType;

$arClasses = array(
    "WialonData" => "lib/wialondata.php",
    '\Newsite\Wialon\MessagesTable' => 'lib/messages.php',
    '\Newsite\Wialon\ObjectsTable' => 'lib/objects.php'
);

\Bitrix\Main\Loader::registerAutoLoadClasses("newsite.wialon", $arClasses);

include ('tools.php');