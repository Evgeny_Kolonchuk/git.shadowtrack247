<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Newsite\Wialon\MessagesTable;
use Newsite\Wialon\ObjectsTable;
use Newsite\Wialon\CustomfieldsTable;
use Newsite\Wialon\SensorsTable;
use Newsite\Wialon\SummaryTable;

Loc::loadMessages(__FILE__);

class newsite_wialon extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'newsite.wialon';
        $this->MODULE_NAME = Loc::getMessage('WIALON_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('WIALON_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('WIALON_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = 'http://newsite.by';
    }

    function getPath($notDocumentRoot = false){
        if($notDocumentRoot){
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        }else{
            return dirname(__DIR__);
        }
    }

    public function doInstall(){
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallFiles();
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
    }
	
	function InstallFiles(){
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/newsite.wialon/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true, true);
	}
	
	function UnInstallFiles(){
		
	}

    public function doUninstall(){
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function installDB(){
        if (Loader::includeModule($this->MODULE_ID))
        {
            MessagesTable::getEntity()->createDbTable();
            ObjectsTable::getEntity()->createDbTable();
            SensorsTable::getEntity()->createDbTable();
            CustomfieldsTable::getEntity()->createDbTable();
            SummaryTable::getEntity()->createDbTable();
        }
    }

    public function uninstallDB(){
        if (Loader::includeModule($this->MODULE_ID))
        {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(MessagesTable::getTableName());
            $connection->dropTable(ObjectsTable::getTableName());
            $connection->dropTable(SensorsTable::getTableName());
            $connection->dropTable(CustomfieldsTable::getTableName());
            $connection->dropTable(SummaryTable::getTableName());
        }
    }
}
