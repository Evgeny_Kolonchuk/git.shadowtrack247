<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['WIALON_TOKEN_TITLE'] = "Токен для доступа к сервису Wialon";
$MESS['WIALON_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['WIALON_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['WIALON_INVALID_VALUE'] = "Введено неверное значение";
