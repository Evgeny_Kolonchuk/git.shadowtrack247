<?php

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$eventManager = Main\EventManager::getInstance();
$eventManager->addEventHandler("main", "OnBuildGlobalMenu", "addItemToGlobalMenu");

function addItemToGlobalMenu(&$aGlobalMenu,&$aModuleMenu){
     global $USER;
     if(!$USER->IsAdmin()){
		return;
	 }     
	
	$aGlobalMenu['global_menu_wialon'] =  array(
										"icon" 			=> "button_websites_support",
										"page_icon" 	=> "page_websites_support",
										"index_icon" 	=> "index_websites_support",
										"text" 			=> Loc::getMessage('WIALON_GLOBAL_MENU_TEXT'),
										"title" 		=> Loc::getMessage('WIALON_GLOBAL_MENU_TITLE'),
										"url" 			=> "websites_support_main.php?lang=".LANGUAGE_ID,
										"sort" 			=> 1000,
										"menu_id" 		=> "wialon",
										"items_id" 		=> "global_menu_wialon",
										"help_section" 	=> "uareports",
										"items" 		=> array()
									);										
		return $aGlobalMenu;
     }



$menu = array(
    array(
        'parent_menu' => 'global_menu_wialon',
        'sort' => 100,
        'text' => Loc::getMessage('WIALON_MENU_TITLE'),
        'title' => Loc::getMessage('WIALON_MENU_TITLE'),
		//"icon" => "default_menu_icon",
		//"page_icon" => "default_page_icon",
        'url' => 'wialon_index.php',
        'items_id' => 'menu_references',
        'items' => array(
            array(
                'text' => Loc::getMessage('WIALON_SUBMENU_TITLE'),
                'url' => 'wialon_index.php?lang=' . LANGUAGE_ID,
                'more_url' => array('wialon_index.php?param1=paramval&lang=' . LANGUAGE_ID),
                'title' => Loc::getMessage('WIALON_SUBMENU_TITLE'),
            ),
        ),
    ),
);

return $menu;