<?php

namespace Newsite\Wialon;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\TextField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class ObjectsTable extends DataManager
{
    public static function getTableName()
    {
        return 'wialon_objects_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => 'ID',
            )),
            new StringField('OBJECT_ID', array(
                'required' => true,
                'title' => 'OBJECT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('OBJECT_NAME', array(
                'required' => false,
                'title' => 'NAME',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TIMESTAMP_START', array(
                'required' => false,
                'title' => 'TIMESTAMP_START',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TIMESTAMP_FINISH', array(
                'required' => false,
                'title' => 'TIMESTAMP_FINISH',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
	        new StringField('PROCESS_TIME', array(
                'required' => false,
                'title' => 'PROCESS_TIME',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('BUSY', array(
                'required' => false,
                'title' => 'BUSY',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            ))
        );
    }
}
