<?php

namespace Newsite\Wialon;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\FloatField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class SensorsTable extends DataManager
{
    public static function getTableName()
    {
        return 'wialon_sensors_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => 'ID',
            )),
            new StringField('OBJECT_ID', array(
                'required' => true,
                'title' => 'OBJECT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('LIGHT', array(
                'required' => false,
                'title' => 'LIGHT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new FloatField('TEMPERATURE', array(
                'required' => false,
                'title' => 'TEMPERATURE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('MOTION', array(
                'required' => false,
                'title' => 'MOTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CONNECTION', array(
                'required' => false,
                'title' => 'CONNECTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new IntegerField('BATTERY', array(
                'required' => false,
                'title' => 'BATTERY',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('STATUS', array(
                'required' => false,
                'title' => 'STATUS',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            ))
        );
    }
}
