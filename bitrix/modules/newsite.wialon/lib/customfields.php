<?php

namespace Newsite\Wialon;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class CustomfieldsTable extends DataManager
{
    public static function getTableName()
    {
        return 'wialon_customfields_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => 'ID',
            )),
            new StringField('OBJECT_ID', array(
                'required' => true,
                'title' => 'OBJECT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DEPARTURE_DATE', array(
                'required' => false,
                'title' => 'DEPARTURE_DATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('ARRIVAL_DATE', array(
                'required' => false,
                'title' => 'ARRIVAL_DATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CUSTOMER', array(
                'required' => false,
                'title' => 'CUSTOMER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('ON_SHIPMENT', array(
                'required' => false,
                'title' => 'ON_SHIPMENT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CARRIER', array(
                'required' => false,
                'title' => 'CARRIER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CARRIER_DISPATCH_NUMBER', array(
                'required' => false,
                'title' => 'CARRIER_DISPATCH_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_ONE', array(
                'required' => false,
                'title' => 'DRIVER_ONE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_TWO', array(
                'required' => false,
                'title' => 'DRIVER_TWO',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_ONE_CONTACT', array(
                'required' => false,
                'title' => 'DRIVER_ONE_CONTACT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_TWO_CONTACT', array(
                'required' => false,
                'title' => 'DRIVER_TWO_CONTACT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('SHIPMENT_ID', array(
                'required' => false,
                'title' => 'SHIPMENT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_NUMBER', array(
                'required' => false,
                'title' => 'TRUCK_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_LICENCE_PLATE', array(
                'required' => false,
                'title' => 'TRUCK_LICENCE_PLATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_DESCRIPTION', array(
                'required' => false,
                'title' => 'TRUCK_DESCRIPTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_NUMBER', array(
                'required' => false,
                'title' => 'TRAILER_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_LICENCE_PLATE', array(
                'required' => false,
                'title' => 'TRAILER_LICENCE_PLATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_DESCRIPTION', array(
                'required' => false,
                'title' => 'TRAILER_DESCRIPTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('ORIGIN', array(
                'required' => false,
                'title' => 'ORIGIN',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DESTINATION', array(
                'required' => false,
                'title' => 'DESTINATION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
        );
    }
}
