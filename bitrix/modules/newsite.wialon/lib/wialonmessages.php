<?php

namespace Newsite\Wialon;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class WialonMessagesTable extends DataManager
{
    public static function getTableName()
    {
        return 'wialon_messages_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('WIALON_ID'),
            )),
            new StringField('OBJECT_ID', array(
                'required' => true,
                'title' => 'OBJECT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('MESSAGE_COUNT', array(
                'required' => false,
                'title' => 'MESSAGE_COUNT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('Y', array(
                'required' => false,
                'title' => 'Широта',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('X', array(
                'required' => false,
                'title' => 'Долгота',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('Z', array(
                'required' => false,
                'title' => 'Высота над уровнем моря',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('S', array(
                'required' => false,
                'title' => 'Скорость',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('COURSE', array(
                'required' => false,
                'title' => 'Курс(направление)',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('SATTELLITE', array(
                'required' => false,
                'title' => 'Спутники',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            ))
        );
    }
}
