<?

class WialonData{

    private $sid = false;
    private $base_api_url = 'https://hst-api.wialon.com/wialon/ajax.html';
    private $module_id = 'newsite.wialon';
    private $token_name = 'wialon_token';
    private $ssid_name = 'wialon_ssid';
    private $userIdName = 'wialon_user_id';
    private $token_lenght = 72;
    private $user_id = false;
    private $bact = false;
    private $defaultAdminEmail = 'a1@newsite.by,kolonchuk@newsite.by';
    private $table_summary_config = array(
    	'OBJECT_ID',
	    'OBJECT_NAME',
	    'DATE_LAST_MSG',
	    'TIMESTAMP',
	    'STATIONARY',
	    'Y',
	    'X',
	    'SPEED',
	    'ADDRESS',
	    'LBS',
	    'LIGHT',
	    'TEMPERATURE',
	    'MOTION',
	    'CONNECTION',
	    'BATTERY',
	    'STATUS',
	    'DEPARTURE_DATE',
	    'ARRIVAL_DATE',
	    'CUSTOMER',
	    'CARRIER',
	    'DRIVER_ONE',
	    'DRIVER_ONE_CONTACT',
	    'DRIVER_TWO',
	    'DRIVER_TWO_CONTACT',
	    'SHIPMENT_ID',
	    'TRUCK_NUMBER',
	    'TRUCK_LICENCE_PLATE',
	    'TRUCK_DESCRIPTION',
	    'TRAILER_NUMBER',
	    'TRAILER_LICENCE_PLATE',
	    'TRAILER_DESCRIPTION',
	    'ORIGIN',
	    'DESTINATION_ONE',
        'DESTINATION_TWO',
	    'CARRIER_DISPATCH_NUMBER',
	    'IN_GEOZONE',
	    'OUT_GEOZONE'
    );
    // template request
    // http://{host}/wialon/ajax.html?sid=<text>&svc=<svc>&params={<params>}
    /// METHODS
    function __construct($scheme = 'https', $host = 'hst-api.wialon.com', $port = '', $sid = false) {
        $this->base_api_url = sprintf('%s://%s%s/wialon/ajax.html?', $scheme, $host, mb_strlen($port) > 0 ? ':'.$port : '');
    }

    function initWialonSession(){
        // обновляем прежнюю сессию
        if(!empty($_REQUEST['access_token']) && !empty($_REQUEST['user_name'])){
            $arParams = array('access_token' => $_REQUEST['access_token'], 'user_name' => $_REQUEST['user_name']);
        }else{
            $arParams = array();
        }
        $_SESSION['WIALON'] = $arParams;
    }


    function validTokenIsset(){
        return strlen($this->getToken()) == (int)$this->getTokenLenght();
    }

    function getModuleId(){
        return $this->module_id;
    }

    function getTokenName(){
        return $this->token_name;
    }

    function getSsidName(){
        return $this->ssid_name;
    }

    function getToken(){
        return COption::GetOptionString($this->getModuleId(),$this->getTokenName());
    }

	function getSsid(){
		return COption::GetOptionString($this->getModuleId(),$this->getSsidName());
	}

    function getUserId(){
	    return COption::GetOptionString($this->getModuleId(),$this->getUserIdName());
    }

	function setUserId($user_id){
		return COption::SetOptionString($this->getModuleId(),$this->getUserIdName(),$user_id);
	}

	function getUserIdName(){
		return $this->userIdName;
	}
    
    function getUserBact(){
        return $this->bact;
    }

    function setToken($token){
        if(strlen($token) != (int)$this->getTokenLenght()){
            return false;
        }
       return COption::SetOptionString($this->getModuleId(),$this->getTokenName(),$token);
    }

	function setSsid($ssid){
		return COption::SetOptionString($this->getModuleId(),$this->getSsidName(),$ssid);
	}

    function getTokenLenght(){
        return $this->token_lenght;
    }

    function getSid(){
        return $this->sid;
    }


    function call($action, $args){

        if(!$this->validTokenIsset()){
            return false;
        }
        
        $url = $this->base_api_url;
        $action = 'svc='.$action;
        $sid = 'sid='.$this->getSid();
        $json = json_encode($args);
        $params = 'params='.$json;
        $str = $action.'&'.$sid.'&'.$params;

        /* cUrl magic */
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => 0, // добавлены 28.02.2017
            CURLOPT_SSL_VERIFYHOST => 0, // добавлены 28.02.2017
            CURLOPT_POSTFIELDS => $str,
	        CURLOPT_SSLVERSION => 1
        );
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if(empty($result)){
            $result = '{"error":-1,"message":'.curl_error($ch).'}';
            $this->setSsid('');
            return $result;
        }

        $result = json_decode($result);

        if ((int)$result->error > 0){
	        $this->setSsid('');
            return WialonError::$errors[$result->error];
        }

        curl_close($ch);

        return $result;
    }

    function getLocationByCoordinates($x=false,$y=false,$uid=false,$flags=1255211008){
        if(empty($x) || empty($y) || empty($uid)){
            return array();
        }
        $coordinates = json_encode(array('lon'=>$x,'lat'=>$y));
        $url = 'https://geocode-maps.wialon.com/hst-api.wialon.com/gis_geocode';
        $str = sprintf('coords=[%s]&flags=%s&uid=%s',$coordinates,$flags,$uid);
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0, // добавлены 28.02.2017
            CURLOPT_SSL_VERIFYHOST => 0, // добавлены 28.02.2017
            CURLOPT_POST => 1,
	        CURLOPT_SSLVERSION => 1,
            CURLOPT_POSTFIELDS => $str
        );
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if($result === false){
            $result = '{"error":-1,"message":'.curl_error($ch).'}';
        }

        curl_close($ch);
        $result = json_decode($result);

        if((int)$result->error > 0){
            return WialonError::$errors[$result->error];
        }

        return $result;
    }

    /* получаем последнее недефолтное значение сенсора */
    function getLastSensorBoolValue($obj_id,$sensId){

	    $timeTo = time();
	    $lastVal = false;

	    if((int)$obj_id <= 0 || (int)$sensId <= 0){
		    return $lastVal;
	    }

	    $action = 'messages/load_last';
	    $args = array(
		    'itemId' => $obj_id,
		    "lastTime" => $timeTo,
		    "flags" => '0x0000',
		    "loadCount" => '0xffffffff', // все сообщения
		    "lastCount" => 1000,
		    "flagsMask"=> '0xFF00'
	    );
	    /* загружаем в сессию последние 1000 сообщений */
	    $this->call($action, $args);

	    $action = 'unit/calc_sensors';
	    $args = array('source'=>'','unitId'=>$obj_id,'indexFrom'=> '0','indexTo'=> '1000','sensorId'=> $sensId);
	    $resultSearch = $this->call($action,$args);

	    if(empty($resultSearch)){
		    return $lastVal;
	    }

	    /* обратная сортировка, потому как в обратном порядке актуальная информация */
	    krsort($resultSearch);

	    foreach ($resultSearch as $objSens){
		    $arrSens = json_decode(json_encode($objSens),1);
		    if($arrSens[$sensId] != -348201.3876){
			    $lastVal = $arrSens[$sensId];
			    $mess = "id obj - {$obj_id}, status value - {$lastVal}";
			    //$this->sendReport($mess);
			    break;
		    }
	    }

	    return $lastVal;
    }

	/* получаем последнее недефолтное значение сенсора */
	function getLastSensorBoolValueByTimeStamp($obj_id,$sensId,$timeFrom = false,$timeTo = false){

		$lastVal = false;

		if((int)$obj_id <= 0 || (int)$sensId <= 0){
			return $lastVal;
		}
		
		$timeTo = $timeTo ? $timeTo :time();
		$timeFrom = $timeFrom ? $timeFrom : AddToTimeStamp(array('MI' => -(60*24*10))); //таймштамп минус x минут от текущего времени

		$this->cleanMess();

		//echo '<pre>'.print_r( date('m-d-Y h:i:s A',$timeTo) ,true).'</pre>';
		//echo '<pre>'.print_r( date('m-d-Y h:i:s A',$timeFrom) ,true).'</pre>';
		$action = 'messages/load_interval';
		$args = array(
			'itemId' => $obj_id,
			"timeFrom" => $timeFrom,
			"timeTo" => $timeTo,
			"flags" => '0x0000',
			"flagsMask"=> '0xFF00',
			"loadCount" => '0xffffffff', // все сообщения
		);

		/* подгружаем в сессию необходимое количество сообщений */
		$this->call($action, $args);

		$action = 'unit/calc_sensors';
		$args = array('source'=>'','unitId'=>$obj_id,'indexFrom'=> '0','indexTo'=> '1000','sensorId'=> $sensId);
		$resultSearch = $this->call($action,$args);

		krsort($resultSearch);

		foreach ($resultSearch as $objSens){
			$arrSens = json_decode(json_encode($objSens),1);

			if($arrSens[$sensId] != -348201.3876){
				$lastVal = $arrSens[$sensId];
				$mess = "id obj - {$obj_id}, status value - {$lastVal}";
				break;
			}
		}

		return $lastVal;
	}

	function cleanDataBeforeSave(&$arAddFields){
		if(empty($arAddFields)){
			return false;
		}
		foreach ($arAddFields as $field => $val){
			if(!in_array($field,$this->table_summary_config)){
				unset($arAddFields[$field]);
			}
		}
		return $arAddFields;
	}

    /* получение списка объектов */
    function getObjectsList($arObjects){

    	if(is_array($arObjects) && count($arObjects) > 0){
    		$findStr = implode(',',$arObjects);
	    }else{
		    $findStr = '*';
	    }

        $args = array(
            'spec' => array(
                "itemsType" => "avl_unit", // avl_unit(объекты)
                "propName" => "sys_id",
                "propValueMask" => $findStr, // поиск всех
                "sortType" => "sys_name",
            ),
            "force" => 1, // 1- поиск заново, 0- вернет прежний результат, если поиск осуществлялся
            "flags" => "0x3FFFFFFFFFFFFFFF",
            "from" => 0, // индекс, начиная с которого осущ поиск
            "to" => 0 // индекс последнего возвращаемого элемента
        );

        $action = 'core/search_items';
        $resultSearch = $this->call($action,$args);

        if($resultSearch instanceof stdClass){
            if(count($resultSearch->items) > 0){
                return json_decode(json_encode($resultSearch->items), 1);
            }
        }
        return false;
    }

    /* возвращает список сенсоров объекта */
    function getSensors($id = false){
        if(!$id){
            return false;
        }

        $args = array(
            'spec' => array(
                "itemsType" => "avl_unit", // avl_unit(объекты)
                "propName" => "sys_id",
                "propValueMask" => $id,
                "sortType" => "sys_name",
            ),
            "force" => 1, // 1- поиск заново, 0- вернет прежний результат, если поиск осуществлялся
            "flags" => "0x3FFFFFFFFFFFFFFF",
            "from" => 0, // индекс, начиная с которого осущ поиск
            "to" => 0 // индекс последнего возвращаемого элемента
        );
        $action = 'core/search_items';
        $resultSearch = $this->call($action,$args);
        if($resultSearch instanceof stdClass){
            if(count($resultSearch->items) > 0){
                $item = current($resultSearch->items);
                return json_decode(json_encode($item->sens), 1);
            }
        }
        return false;
    }

    function cleanMess(){
	    $action = 'messages/unload';
	    $args = array();

	    return $this->call($action, $args);
    }

    /* последние сообщения объектов */
    function getLastMess($id = false){
        if(!$id){
            return false;
        }
        $timeTo = time();
        $timeFrom = AddToTimeStamp(array('MI' => -600)); //таймштамп минус x минут от текущего времени
        $action = 'messages/load_interval';
        $args = array(
            'itemId' => $id,
            "timeFrom" => $timeFrom,
            "flags" => 1,
            "loadCount" => '0xffffffff', // все сообщения
            "timeTo" => $timeTo,
            "flagsMask"=> 65281
        );
        $resultSearch = $this->call($action, $args);

        if($resultSearch instanceof stdClass){
            return json_decode(json_encode($resultSearch), 1);
        }
        return false;
    }

    /* получает список уведомлений для данной сессии */
    function getEvents(){

        $url = 'https://hst-api.wialon.com/avl_evts?sid='.$this->getSid();
        $str = '';
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0, // добавлены 28.02.2017
            CURLOPT_SSL_VERIFYHOST => 0, // добавлены 28.02.2017
            CURLOPT_POST => 1,
	        CURLOPT_SSLVERSION => 1,
            CURLOPT_POSTFIELDS => $str
        );
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if($result === false){
            $result = '{"error":-1,"message":'.curl_error($ch).'}';
        }

        curl_close($ch);
        $result = json_decode($result);

        if((int)$result->error > 0){
            return WialonError::$errors[$result->error];
        }
        return $result;
    }

    /* получаем данные уведомлений по id */
    function getNotificationData($id=false){
        $result = false;
        if((int)$id <= 0){
            return $result;
        }

        /* получаем список пользователей */
        $args = array(
            'spec' => array(
                "itemsType" => "avl_resource", // user(пользователи), avl_unit(объекты), avl_route, avl_retranslator, avl_resource(подробная информация по пользоавтелям)
                "propName" => "sys_id",
                "propValueMask" => "*",
                "sortType" => "sys_name",
            ),
            "force" => 1, // 1- поиск заново, 0- вернет прежний результат, если поиск осуществлялся
            "flags" => "0x3FFFFFFFFFFFFFFF",
            "from" => 0, // индекс, начиная с которого осущ поиск
            "to" => 0 // индекс последнего возвращаемого элемента
        );
        $action = 'core/search_items';
        $resultSearch = $this->call($action,$args);

        foreach ($resultSearch->items as $item){
            $action = 'resource/get_notification_data';
            $args = array('itemId'=> $item->id,'col'=> array($id));
            $result = $this->call($action,$args);        
        }

        return $result;
    }

    /* получаем список геозон */
    function getGeozones(){
        $args = array(
            'spec' => array(
                "itemsType" => "avl_resource", // user(пользователи), avl_unit(объекты), avl_route, avl_retranslator, avl_resource(подробная информация по пользоавтелям)
                "propName" => "sys_id",
                "propValueMask" => "*",
                "sortType" => "sys_name",
            ),
            "force" => 1, // 1- поиск заново, 0- вернет прежний результат, если поиск осуществлялся
            "flags" => "0x3FFFFFFFFFFFFFFF",
            "from" => 0, // индекс, начиная с которого осущ поиск
            "to" => 0 // индекс последнего возвращаемого элемента
        );
        $action = 'core/search_items';
        $resultSearch = $this->call($action,$args);
        $arGeozones = array();
        foreach ($resultSearch->items as $item){
            if(!empty($item->zl)){
                foreach ($item->zl as $zone){
                    $arGeozones[$zone->id] = json_decode(json_encode($zone),1);
                }
            }
        }
        return $arGeozones;
    }

    function sessIdUpdate(){
        if(!$this->validTokenIsset()){
            return false;
        }

        $ssid = $this->getSsid();
        $user_id = $this->getUserId();

        if(empty($ssid) || empty($user_id)){
	        $args = array('token' => $this->getToken());
	        $resultLogin = $this->call('token/login',$args);
	        if($resultLogin instanceof \stdClass){
		        $this->sid = !empty($resultLogin->eid) ? $resultLogin->eid : false;
		        $this->setSsid($this->sid);

		        $this->signSession(); // подписываем сессию

		        $this->user_id = !empty($resultLogin->user->id) ? $resultLogin->user->id : false;
		        $this->setUserId($this->user_id);

		        //$this->bact = !empty($resultLogin->user->bact) ? $resultLogin->user->bact : false;

	        }else{
		        $this->sendReport($resultLogin);
		        sleep(2);
	        }
        }else{
	        $this->sid = $ssid;
	        $this->user_id = $user_id;
        }
    }

    function sendReport($message,$mail=false){
        $sendTo = $this->defaultAdminEmail;
        if($mail){
            $sendTo = $mail;
        }
        $headers = 'From: shadowtrack247@newsite.by' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        return mail($sendTo, 'report Wialon', $message, $headers);
    }

    function getSensorsData($id = false){
        if(!$id){
            return false;
        }
        $action = 'unit/calc_last_message';
        $args = ['unitId' => $id,'sensors'=>[]];
        $resultSearch = $this->call($action, $args);

        if($resultSearch instanceof \stdClass){
            return json_decode(json_encode($resultSearch), 1);
        }
        return false;
    }

    /* подписываем сессию на события */
	function signSession(){
		$action = 'core/update_data_flags';
		$dataSend = ['spec' =>
			[
				[
					'type' => 'type', // подписка по типу
					'data' => 'avl_resource', // пользователи
					'flags' => 0x0600, // тип уведомления
					'mode' => 0 // перезаписываем (не обновляем)
				]
			]
		];
		return $this->call($action,$dataSend);
	}

	function translitCode($name,$mode = 'L'){
		$params = array(
			"max_len" => "100", // обрезает символьный код до 100 символов
			"change_case" => $mode, // буквы преобразуются к определенному регистру
			"replace_space" => "_", // меняем пробелы на нижнее подчеркивание
			"replace_other" => "_", // меняем левые символы на нижнее подчеркивание
			"delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
			"use_google" => "false" // отключаем использование google
		);
		return CUtil::translit($name, "ru" , $params);
	}


    function getTableInfo($classTableName=false,$filter=false,$arSelect=false,$limit=false,$orderBy = false){

        if(empty($classTableName)){
            return;
        }
        $query = new \Bitrix\Main\Entity\Query( call_user_func($classTableName.'::getEntity',false));

        if(!$arSelect){
            $arSelect = array('*');
        }

        if($filter){
            $query->setFilter($filter);
        }

        if((int)$limit > 0){
            $query->setLimit($limit);
        }

	    if($orderBy){
		    $query->setOrder($orderBy);
	    }

        $query->setSelect($arSelect);
        $resultQuery = $query->exec();
        $arResult = array();
        while ($line = $resultQuery->fetch()){
            $arResult[$line['ID']] = $line;
        }
        return count($arResult) > 0 ? $arResult : false;
    }

    function addTableLine($classTableName=false,$arData=false){
        if(!$arData || empty($classTableName)){
            return false;
        }

        $resultQuery = call_user_func($classTableName.'::add',$arData);
        //echo '<pre>'.print_r($resultQuery->getErrorMessages(),true).'</pre>';
        return $resultQuery->isSuccess();
    }

    function updateTableLine($classTableName=false,$primaryKey,$arUpdateFields){
        if(!$arUpdateFields || empty($classTableName)){
            return false;
        }

        $resultQuery = call_user_func($classTableName.'::update',$primaryKey,$arUpdateFields);
	    //echo '<pre>'.print_r($resultQuery->getErrorMessages(),true).'</pre>';
        return $resultQuery->isSuccess();
    }

    function deleteTableLine($classTableName=false,$primaryKey=false){
        if(!$primaryKey || empty($classTableName)){
            return false;
        }

        $resultQuery = call_user_func($classTableName.'::delete',$primaryKey);
        return $resultQuery->isSuccess();
    }
}


class WialonError{
    /// PROPERTIES
    /** list of error messages with codes */
    public static $errors = array(
        1 => 'Invalid session',
        2 => 'Invalid service',
        3 => 'Invalid result',
        4 => 'Invalid input',
        5 => 'Error performing request',
        6 => 'Unknow error',
        7 => 'Access denied',
        8 => 'Invalid user name or password',
        9 => 'Authorization server is unavailable, please try again later',
        1001 => 'No message for selected interval',
        1002 => 'Item with such unique property already exists',
        1003 => 'Only one request of given time is allowed at the moment'
    );

    /// METHODS
    /** error message generator */
    public static function error($code = '', $text = ''){
        $code = intval($code);
        if ( isset(self::$errors[$code]) )
            $text = self::$errors[$code].' '.$text;
        $message = sprintf('%d: %s', $code, $text);
        return sprintf('WialonError( %s )', $message);
    }
}