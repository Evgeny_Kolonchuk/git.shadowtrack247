<?php

namespace Newsite\Wialon;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Entity\FloatField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class SummaryTable extends DataManager
{
    public static function getTableName()
    {
        return 'wialon_summary_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => 'ID',
            )),
            new StringField('OBJECT_ID', array(
                'required' => true,
                'title' => 'OBJECT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('OBJECT_NAME', array(
                'required' => false,
                'title' => 'NAME',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
	        new DatetimeField('DATE_LAST_MSG', array(
		        'required' => false,
		        'title' => 'DATE_LAST_MSG',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
	        )),
            new StringField('TIMESTAMP', array(
                'required' => false,
                'title' => 'TIMESTAMP',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('POSITION', array(
                'required' => false,
                'title' => 'POSITION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new FloatField('X', array(
                'required' => false,
                'title' => 'X',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new FloatField('Y', array(
                'required' => false,
                'title' => 'Y',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('STATIONARY', array(
                'required' => false,
                'title' => 'STATIONARY',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('SPEED', array(
                'required' => false,
                'title' => 'SPEED',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('LBS', array(
                'required' => false,
                'title' => 'LBS',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('LIGHT', array(
                'required' => false,
                'title' => 'LIGHT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new FloatField('TEMPERATURE', array(
                'required' => false,
                'title' => 'TEMPERATURE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('MOTION', array(
                'required' => false,
                'title' => 'MOTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CONNECTION', array(
                'required' => false,
                'title' => 'CONNECTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new IntegerField('BATTERY', array(
                'required' => false,
                'title' => 'BATTERY',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DEPARTURE_DATE', array(
                'required' => false,
                'title' => 'DEPARTURE_DATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('ARRIVAL_DATE', array(
                'required' => false,
                'title' => 'ARRIVAL_DATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CUSTOMER', array(
                'required' => false,
                'title' => 'CUSTOMER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CARRIER', array(
                'required' => false,
                'title' => 'CARRIER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_ONE', array(
                'required' => false,
                'title' => 'DRIVER_ONE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_ONE_CONTACT', array(
                'required' => false,
                'title' => 'DRIVER_ONE_CONTACT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('CARRIER_DISPATCH_NUMBER', array(
                'required' => false,
                'title' => 'CARRIER_DISPATCH_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_TWO', array(
                'required' => false,
                'title' => 'DRIVER_TWO',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DRIVER_TWO_CONTACT', array(
                'required' => false,
                'title' => 'DRIVER_TWO_CONTACT',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('SHIPMENT_ID', array(
                'required' => false,
                'title' => 'SHIPMENT_ID',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_NUMBER', array(
                'required' => false,
                'title' => 'TRUCK_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_LICENCE_PLATE', array(
                'required' => false,
                'title' => 'TRUCK_LICENCE_PLATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRUCK_DESCRIPTION', array(
                'required' => false,
                'title' => 'TRUCK_DESCRIPTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_NUMBER', array(
                'required' => false,
                'title' => 'TRAILER_NUMBER',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_LICENCE_PLATE', array(
                'required' => false,
                'title' => 'TRAILER_LICENCE_PLATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('TRAILER_DESCRIPTION', array(
                'required' => false,
                'title' => 'TRAILER_DESCRIPTION',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('ORIGIN', array(
                'required' => false,
                'title' => 'ORIGIN',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('STATUS', array(
                'required' => false,
                'title' => 'STATUS',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('DESTINATION_ONE', array(
                'required' => false,
                'title' => 'DESTINATION_ONE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
             new StringField('DESTINATION_TWO', array(
                'required' => false,
                'title' => 'DESTINATION_TWO',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
             new StringField('DOOR_STATE', array(
                'required' => false,
                'title' => 'DOOR_STATE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
             new StringField('CHARGING', array(
                'required' => false,
                'title' => 'CHARGING',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('IN_GEOZONE', array(
                'required' => false,
                'title' => 'IN_GEOZONE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new StringField('OUT_GEOZONE', array(
                'required' => false,
                'title' => 'OUT_GEOZONE',
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            ))
        );
    }
}
