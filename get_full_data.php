<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("get_full_data.php");
?>
<?if (!CModule::IncludeModule("newsite.wialon")) {
    return false;
}

$wialon_api = new WialonData();
$wialon_api->sessIdUpdate();

$arObjects = $wialon_api->getTableInfo('Newsite\Wialon\SummaryTable',false,false,false,['TIMESTAMP'=>'ASC']);
/*
foreach ($arObjects as $key => $obj){

	$arObjects[$obj['TIMESTAMP']] = $obj;
	$arObjects[$obj['TIMESTAMP']]['TIMESTAMP'] = date('Y-m-d H:i:s',$obj['TIMESTAMP']);
	unset($arObjects[$key]);
}
ksort($arObjects);
*/
?>
<table>
    <tr>
        <td>OBJECT_ID</td>
        <td>OBJECT_NAME</td>
        <td>DATE_LAST_MSG</td>
        <td>TIMESTAMP</td>
        <td>STATIONARY</td>
        <td>X</td>
        <td>Y</td>
        <td>SPEED</td>
        <td>ADDRESS</td>
        <td>LBS</td>
        <td>LIGHT</td>
        <td>TEMPERATURE</td>
        <td>MOTION</td>
        <td>CONNECTION</td>
        <td>BATTERY</td>
        <td>STATUS</td>
        <td>DEPARTURE_DATE</td>
        <td>ARRIVAL_DATE</td>
        <td>CUSTOMER</td>
        <td>CARRIER</td>
        <td>DRIVER_ONE</td>
        <td>DRIVER_ONE_CONTACT</td>
        <td>DRIVER_TWO</td>
        <td>DRIVER_TWO_CONTACT</td>
        <td>SHIPMENT_ID</td>
        <td>TRUCK_NUMBER</td>
        <td>TRUCK_LICENCE_PLATE</td>
        <td>TRUCK_DESCRIPTION</td>
        <td>TRAILER_NUMBER</td>
        <td>TRAILER_LICENCE_PLATE</td>
        <td>TRAILER_DESCRIPTION</td>
        <td>ORIGIN</td>
        <td>DESTINATION_ONE</td>
        <td>DESTINATION_TWO</td>
        <td>CARRIER_DISPATCH_NUMBER</td>

        <td>DOOR_STATE</td>
        <td>CHARGING</td>

        <td>IN_GEOZONE</td>
        <td>OUT_GEOZONE</td>
    </tr>

    <?foreach ($arObjects as $obj):?>
        <tr>
            <td><?=$obj['OBJECT_ID'] != '-348201.3876' ? $obj['OBJECT_ID'] : ''?></td>
            <td><?=$obj['OBJECT_NAME'] != '-348201.3876' ? $obj['OBJECT_NAME'] : ''?></td>
            <td><?=$obj['DATE_LAST_MSG']?></td>
            <td><?=$obj['TIMESTAMP']?></td>
            <td><?=$obj['STATIONARY'] != '-348201.3876' ? $obj['STATIONARY'] : ''?></td>
            <td><?=$obj['X'] != '-348201.3876' ? $obj['X'] : ''?></td>
            <td><?=$obj['Y'] != '-348201.3876' ? $obj['Y'] : ''?></td>
            <td><?=$obj['SPEED'] != '-348201.3876' ? $obj['SPEED'] : ''?></td>
            <td><?=$obj['POSITION'] != '-348201.3876' ? $obj['POSITION'] : ''?></td>
            <td><?=$obj['LBS']?><?=$obj['LBS'] != '-348201.3876' ? $obj['LBS'] : ''?></td>
            <td><?=$obj['LIGHT'] != '-348201.3876' ? $obj['LIGHT'] : ''?></td>
            <td><?=$obj['TEMPERATURE'] != '-348201.3876' ? $obj['TEMPERATURE'] : ''?></td>
            <td><?=$obj['MOTION'] != '-348201.3876' ? $obj['MOTION'] : ''?></td>
            <td><?=$obj['CONNECTION'] != '-348201.3876' ? $obj['CONNECTION'] : ''?></td>
            <td><?=$obj['BATTERY'] != '-348201.3876' ? $obj['BATTERY'] : ''?></td>
            <td><?=$obj['STATUS'] != '-348201.3876' ? $obj['STATUS'] : ''?></td>
            <td><?=$obj['DEPARTURE_DATE'] != '-348201.3876' ? $obj['DEPARTURE_DATE'] : ''?></td>
            <td><?=$obj['ARRIVAL_DATE'] != '-348201.3876' ? $obj['ARRIVAL_DATE'] : ''?></td>
            <td><?=$obj['CUSTOMER'] != '-348201.3876' ? $obj['CUSTOMER'] : ''?></td>
            <td><?=$obj['CARRIER'] != '-348201.3876' ? $obj['CARRIER'] : ''?></td>
            <td><?=$obj['DRIVER_ONE'] != '-348201.3876' ? $obj['DRIVER_ONE'] : ''?></td>
            <td><?=$obj['DRIVER_ONE_CONTACT']?></td>
            <td><?=$obj['DRIVER_TWO']?></td>
            <td><?=$obj['DRIVER_TWO_CONTACT']?></td>
            <td><?=$obj['SHIPMENT_ID']?></td>
            <td><?=$obj['TRUCK_NUMBER']?></td>
            <td><?=$obj['TRUCK_LICENCE_PLATE']?></td>
            <td><?=$obj['TRUCK_DESCRIPTION']?></td>
            <td><?=$obj['TRAILER_NUMBER']?></td>
            <td><?=$obj['TRAILER_LICENCE_PLATE']?></td>
            <td><?=$obj['TRAILER_DESCRIPTION']?></td>
            <td><?=$obj['ORIGIN']?></td>
            <td><?=$obj['DESTINATION_ONE']?></td>
            <td><?=$obj['DESTINATION_TWO']?></td>
            <td><?=$obj['CARRIER_DISPATCH_NUMBER']?></td>
            <td><?=$obj['DOOR_STATE']?></td>
            <td><?=$obj['CHARGING']?></td>
            <td><?=$obj['IN_GEOZONE']?></td>
            <td><?=$obj['OUT_GEOZONE']?></td>
        </tr>
    <?endforeach;?>
</table>

<style type="text/css">
    table {
        border-collapse: collapse; /* Убираем двойные линии между ячейками */
    }
    td {
        padding: 10px; /* Поля вокруг содержимого таблицы */
        border: 1px solid black; /* Параметры рамки */
        background: #ffe872; /* Цвет фона */
    }
</style>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
