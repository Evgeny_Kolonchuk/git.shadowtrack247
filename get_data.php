<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("get_data.php");
?>
<?if (!CModule::IncludeModule("newsite.wialon")) {
    return false;
}

$wialon_api = new WialonData();
$wialon_api->sessIdUpdate();

$arObjects = $wialon_api->getTableInfo('Newsite\Wialon\ObjectsTable');
?>
<table>
    <tr>
        <td>TIMESTAMP</td>
        <td>OBJECT_ID</td>
        <td>OBJECT_NAME</td>
        <td>DATE_LAST_MSG</td>
        <td>X</td>
        <td>Y</td>
        <td>Stationary</td>
        <td>SPEED</td>
        <td>COURSE</td>
        <td>ADDRESS</td>
        <td>LBS</td>
        <td>CUSTOM_FIELDS</td>
        <td>SENSORS_DATA</td>
    </tr>

<?foreach ($arObjects as $obj):?>
    <?$arCustomFields = json_decode($obj['CUSTOM_FIELDS']);?>
    <?$arSensorsData = json_decode($obj['SENSORS_DATA'])?>
    <tr>
        <td><?=$obj['TIMESTAMP']?></td>
        <td><?=$obj['OBJECT_ID']?></td>
        <td><?=$obj['OBJECT_NAME']?></td>
        <td><?=$obj['DATE_LAST_MSG']?></td>
        <td><?=$obj['X']?></td>
        <td><?=$obj['Y']?></td>
        <td><?=(int)($obj['SPEED'] > 0)?></td>
        <td><?=$obj['SPEED']?></td>
        <td><?=$obj['COURSE']?></td>
        <td><?=$obj['POSITION']?></td>
        <td><?=$obj['LBS']?></td>
        <td>

            <?if($arCustomFields instanceof stdClass){
                echo '<table>';
                foreach ($arCustomFields as $arField){
                    echo '<tr>';
                    echo '<td>'.$arField->NAME.'</td><td>'.$arField->VALUE.'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }?>

        </td>
        <td>

            <?if($arSensorsData instanceof stdClass){
                echo '<table>';
                foreach ($arSensorsData as $arSensor){
                    echo '<tr>';
                    echo '<td>'.$arSensor->NAME.'</td><td>'.$arSensor->VALUE.'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }?>
            
        </td>
        <?/**?>
        <td>
            <?$arNotificationData = json_decode($obj['NOTIFICATION_DATA'])?>
            <?if($arNotificationData instanceof stdClass){
                echo '<table>';
                echo '<tr><td>EVENT</td><td>'.$arNotificationData->EVENT.'</td></tr>';
                echo '<tr><td>TIMESTAMP</td><td>'.$arNotificationData->TIMESTAMP.'</td></tr>';
                echo '<tr><td>COUNT_MESS</td><td>'.$arNotificationData->MESS_NUMBER.'</td></tr>';
                echo '</table>';
            }?>
        </td>
        <?/**/?>
    </tr>
<?endforeach;?>
</table>
<style type="text/css">
    table {
        border-collapse: collapse; /* Убираем двойные линии между ячейками */
    }
    td {
        padding: 10px; /* Поля вокруг содержимого таблицы */
        border: 1px solid black; /* Параметры рамки */
        background: #ff7272; /* Цвет фона */
    }
</style>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
