<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Login");
?>
<?if (!CModule::IncludeModule("newsite.wialon")){
    die('Модуль newsite.wialon не подключен');
}?>
<?
$resTokenUpd = false;
?>
<?if(!empty($_POST['access_token']) && !empty($_POST['save_token'])){
    $wialon_api = new WialonData();
    $resTokenUpd = $wialon_api->setToken($_POST['access_token']);
}?>
<?if($resTokenUpd) :?>
    Вы успешно сохранили token доступа к данным Wialon.
<?elseif(!empty($_GET['access_token'])):?>
    Вы получили token для полного доступа к ресурсами Wialon. <br/>
    Можете его сохранить в настройках модуля, для последующей работы.

    <form id="access_token_form" method="post" action="#">
        <input type="text" value="<?=$_GET['access_token']?>" name="access_token">
        <input type="submit" value="Сохранить token" name="save_token">
    </form>
<?else:?>
    <script>
        wialonObj.initParams();
    </script>
    <input id="login" type="button" value="Авторизоваться" onclick="wialonObj.getToken()"/>
<?endif;?>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
