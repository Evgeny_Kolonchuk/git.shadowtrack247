<?php
$_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

//use Bitrix\Main\Type;


if (!CModule::IncludeModule("newsite.wialon")) {
    return false;
}

$time = time();

$file = $_SERVER['DOCUMENT_ROOT'] . '/_log.txt';
//fwrite(fopen($file, "a"), "Start ".date('Y-m-d H:i:s',time()));


$wialon_api = new WialonData();

$wialon_api->sessIdUpdate();

$objList = $wialon_api->getObjectsList();
if(empty($objList)){
	return ;
}
$exec0 = microtime(true) - $time;
//echo '<pre>'.print_r( "exec #0 {$exec0}" ,true).'</pre>';
/* подписываем сессию на объекты и передаем в нее события */
$action = 'core/update_data_flags';
$dataSend = ['spec' => [
                            [
                                'type' => 'type', // подписка по типу
                                'data' => 'avl_resource', // пользователи
                                'flags' => 0x0600, // тип уведомления
                                'mode' => 0 // перезаписываем (не обновляем)
                            ]
                        ]
            ];
$resAdd = $wialon_api->call($action,$dataSend);

/* получаем уведомления */
$resultNotifications = $wialon_api->getEvents();

/* нужен для обновления таблицы, если объект не пришел, удаляем его после */
$arObjId = array();

/* получаем список геозон */
$arGeozones = $wialon_api->getGeozones();

/* данные необходимы для обхода таблиц и удаления неактуальных данных */
$arFullTablaObjects = $wialon_api->getTableInfo('\Newsite\Wialon\SummaryTable',array());
foreach ($arFullTablaObjects as $id => $line){
	$arFullTablaObjects[$line['OBJECT_ID']] = $line;
	unset($arFullTablaObjects[$id]);
}

$arSimpleTablaObjects = $wialon_api->getTableInfo('\Newsite\Wialon\ObjectsTable',array());
foreach ($arSimpleTablaObjects as $id => $line){
	$arSimpleTablaObjects[$line['OBJECT_ID']] = $line;
	unset($arSimpleTablaObjects[$id]);
}

$exec1 = microtime(true) - $time;
//echo '<pre>'.print_r( "exec #1 {$exec1}" ,true).'</pre>';

$counter = 1;
foreach ($objList as $k => $obj){

	//$wialon_api->cleanMess();

    $arResultFull = array();
    $timestamp = time();
    $arAddFields = array(
        'OBJECT_ID' => $obj['id'],
        'OBJECT_NAME' => $obj['nm'],
        'TIMESTAMP' => date('d.m.Y H:i:s',time())//new \Bitrix\Main\Type\DateTime( date('d.m.Y H:i:s',time()) )
    );

    //print_r(ConvertTimeStamp( time(), "FULL"));
    //print_r($arAddFields);die();



    if(is_array($obj['flds']) && count($obj['flds']) > 0){

        $arCustomFieldsDataForTable = array();
        foreach ($obj['flds'] as $customField){
            $arCustoms[$customField['id']] = array('NAME'=>$customField['n'],'VALUE'=>$customField['v']);
            switch ($customField['n']){
                case 'Departure Date':                
                    if(!empty($customField['v'])){
                        $arCustomFieldsDataForTable['DEPARTURE_DATE'] = $customField['v'];
                        //echo $customField['v'] . PHP_EOL;
                        $ar_time = explode('/', $customField['v']);
                        if(strlen($ar_time[0]) < 2){
                            $ar_time[0] = str_pad($ar_time[0],2,'0',STR_PAD_LEFT);                         
                        }
                        if(strlen($ar_time[1]) < 2){
                            $ar_time[1] = str_pad($ar_time[1],2,'0',STR_PAD_LEFT);                         
                        }                          
                        $customField['v'] = implode('/',$ar_time);         

                        $time = new \Bitrix\Main\Type\DateTime( date('d.m.Y H:i:s',strtotime($customField['v'])) );
                    }else{
                        $time = false;
                    }
                    //$arCustomFieldsDataForTable['DEPARTURE_DATE'] = $time;
                    
                    break;
                case 'Arrival Date':

                     if(!empty($customField['v'])){
                        $arCustomFieldsDataForTable['ARRIVAL_DATE'] = $customField['v'];
                        //echo $customField['v'] . PHP_EOL;
                        $ar_time = explode('/', $customField['v']);
                        if(strlen($ar_time[0]) < 2){
                            $ar_time[0] = str_pad($ar_time[0],2,'0',STR_PAD_LEFT);                         
                        }
                        if(strlen($ar_time[1]) < 2){
                            $ar_time[1] = str_pad($ar_time[1],2,'0',STR_PAD_LEFT);                         
                        }                          
                        $customField['v'] = implode('/',$ar_time);      

                        $time = new \Bitrix\Main\Type\DateTime( date('d.m.Y H:i:s',strtotime($customField['v'])) );
                    }else{
                        $time = false;
                    }
                    //$arCustomFieldsDataForTable['ARRIVAL_DATE'] = $time;
                    
                    break;
                case 'Customer':
                    $arCustomFieldsDataForTable['CUSTOMER'] = $customField['v'];
                    break;
                case 'Carrier':
                    $arCustomFieldsDataForTable['CARRIER'] = $customField['v'];
                    break;
                case '1 Driver':
                    $arCustomFieldsDataForTable['DRIVER_ONE'] = $customField['v'];
                    break;
                case '2 Driver':
                    $arCustomFieldsDataForTable['DRIVER_TWO'] = $customField['v'];
                    break;
                case 'Contact 1 Driver':
                    $arCustomFieldsDataForTable['DRIVER_ONE_CONTACT'] = $customField['v'];
                    break;
                case 'Contact 2 Driver':
                    $arCustomFieldsDataForTable['DRIVER_TWO_CONTACT'] = $customField['v'];
                    break;
                case 'Shipment ID #':
                    $arCustomFieldsDataForTable['SHIPMENT_ID'] = $customField['v'];
                    break;
                case 'Shipment ID':
                    $arCustomFieldsDataForTable['SHIPMENT_ID'] = $customField['v'];
                    break;
                case 'Truck Number':
                    $arCustomFieldsDataForTable['TRUCK_NUMBER'] = $customField['v'];
                    break;
                case 'Truck Licence Plate':
                    $arCustomFieldsDataForTable['TRUCK_LICENCE_PLATE'] = $customField['v'];
                    break; 
                case 'Carrier Dispatch Number':
                    $arCustomFieldsDataForTable['CARRIER_DISPATCH_NUMBER'] = $customField['v'];
                    break;
                case 'Truck Description':
                    $arCustomFieldsDataForTable['TRUCK_DESCRIPTION'] = $customField['v'];
                    break;
                case 'Trailer Number':
                    $arCustomFieldsDataForTable['TRAILER_NUMBER'] = $customField['v'];
                    break;
                case 'Trailer License Plate':
                    $arCustomFieldsDataForTable['TRAILER_LICENCE_PLATE'] = $customField['v'];
                    break;
                case 'Trailer Description':
                    $arCustomFieldsDataForTable['TRAILER_DESCRIPTION'] = $customField['v'];
                    break;
                case 'Origin':
                    $arCustomFieldsDataForTable['ORIGIN'] = $customField['v'];
                    break;
                case 'Destination one':
                    $arCustomFieldsDataForTable['DESTINATION_ONE'] = $customField['v'];
                    break;
                case 'Destination two':
                    $arCustomFieldsDataForTable['DESTINATION_TWO'] = $customField['v'];
                    break;
            }
        }
        if(count($arCustomFieldsDataForTable) > 0){

            $arResultFull = array_merge($arResultFull,$arCustomFieldsDataForTable);
            $arCustomFieldsDataForTable['OBJECT_ID'] = $obj['id'];

            /* проверка существует ли объект, если да, то апдейтим, если нет, то создаем новую запись */
            $resQueryCustomFieldsTable = $wialon_api->getTableInfo('\Newsite\Wialon\CustomfieldsTable',array('OBJECT_ID' => $obj['id']));
            if(empty($resQueryCustomFieldsTable)){
                $resAdd = $wialon_api->addTableLine('\Newsite\Wialon\CustomfieldsTable',$arCustomFieldsDataForTable);
            }else{
                $id = key($resQueryCustomFieldsTable);
                $resUpd = $wialon_api->updateTableLine('\Newsite\Wialon\CustomfieldsTable',$id,$arCustomFieldsDataForTable);
                //echo '<pre>'.print_r( $arCustomFieldsDataForTable ,true).'</pre>';
            }
        }
        $arAddFields['CUSTOM_FIELDS'] = json_encode($arCustoms);
    }
    // $obj['lmsg']['p']['cell_id'] флаг определяющий источник информации доступные в этих сообщениях 48412 и 30071
    // если позиция определена по LBS, то будет приъодить параметр, если по GPS, то будет пустым (ответ поддержки)
    
    if(is_array($obj['lmsg']['pos']) && !empty($obj['lmsg']['pos'])){
        $arAddFields['Y'] = $obj['lmsg']['pos']['y'];
        $arAddFields['X'] = $obj['lmsg']['pos']['x'];
        $arAddFields['Z'] = $obj['lmsg']['pos']['z'];
        $arAddFields['SPEED'] = $obj['lmsg']['pos']['s'];
        $arAddFields['COURSE'] = $obj['lmsg']['pos']['c'];
        $arAddFields['DATE_LAST_MSG'] = new \Bitrix\Main\Type\DateTime( date('d.m.Y H:i:s',$obj['lmsg']['t']) );
        $location = $wialon_api->getLocationByCoordinates($arAddFields['X'],$arAddFields['Y'],$wialon_api->getUserId());
        $position = array_shift($location);
        $arAddFields['POSITION'] = !empty($position) ? $position : '';
    }

    $arAddFields['LBS'] = (int)$obj['lmsg']['p']['cell_id'];
    $arResultFull = array_merge($arResultFull,$arAddFields);
    $arResultFull['STATIONARY'] = $arAddFields['SPEED'] == 0;

    /* тянем список сенсоров */
    $arSensors = $wialon_api->getSensors($obj['id']);

    /* тянем значения сенсоров, порядковые номера совпадают */
    $arSensorData = $wialon_api->getSensorsData($obj['id']);

    if(is_array($arSensors) && count($arSensors) > 0){
        $arSensorDataForTable = array();

        foreach ($arSensors as $num => $sens){

	        if(strtolower($sens['n']) == 'status'){
		        $status = $wialon_api->getLastSensorBoolValueByTimeStamp($obj['id'],$sens['id'],(int)$arFullTablaObjects[$obj['id']]['TIMESTAMP']);
		        if($status !== false){
			        $arSensorDataForTable['STATUS'] = $status;
		        }
	        }


            if($arSensorData[$num] == -348201.3876){
	            $arSensorData[$num] = '';
                //continue;
            }


            $arSensResult[$num] = array('NAME'=>$sens['n'],'VALUE'=>$arSensorData[$num]);
            /* формируем массив для добавления в таблицу сенсоров */
            switch (strtolower($sens['n'])){
                case 'battery':
                    $arSensorDataForTable['BATTERY'] = $arSensorData[$num];
                    break;
                case 'temperature':
                    $arSensorDataForTable['TEMPERATURE'] = $arSensorData[$num];
                    break;
                case 'light':
                    $arSensorDataForTable['LIGHT'] = $arSensorData[$num];
                    break;
                case 'motion':
                    $arSensorDataForTable['MOTION'] = $arSensorData[$num];
                    break;
                case 'connection':
                    $arSensorDataForTable['CONNECTION'] = $arSensorData[$num];
                    break;
                case 'door state':
                    if($arSensorData[$num] == 0){
                        $arSensorDataForTable['DOOR_STATE'] = 'Unlocked';
                    }elseif ($arSensorData[$num] == 1) {
                        $arSensorDataForTable['DOOR_STATE'] = 'Locked';
                    }                    
                    break;
                case 'charging':
                    if($arSensorData[$num] == 0){
                        $arSensorDataForTable['CHARGING'] = 'Not charging';
                    }elseif ($arSensorData[$num] == 1) {
                        $arSensorDataForTable['CHARGING'] = 'Charging';
                    }                    
                    break;
            }
        }

        if(count($arSensorDataForTable) > 0){

            $arResultFull = array_merge($arResultFull,$arSensorDataForTable);
            $arSensorDataForTable['OBJECT_ID'] = $obj['id'];

            /* проверка существует ли объект, если да, то апдейтим, если нет, то создаем новую запись */
            $resQuerySensorsTable = $wialon_api->getTableInfo('\Newsite\Wialon\SensorsTable',array('OBJECT_ID' => $obj['id']));
            if(empty($resQuerySensorsTable)){
                //$resAdd = $wialon_api->addTableLine('\Newsite\Wialon\SensorsTable',$arSensorDataForTable);
            }else{
                $id = key($resQuerySensorsTable);
                //$resUpd = $wialon_api->updateTableLine('\Newsite\Wialon\SensorsTable',$id,$arSensorDataForTable);
            }
        }

        $arAddFields['SENSORS_DATA'] = json_encode($arSensResult);
    }

    /* из списка тянем данные по текущему объекту */
    $arNotification = array();
    $lastNotif = array();
    $arDataNotif = array();

    $arResultFull['IN_GEOZONE'] = '';
    $arResultFull['OUT_GEOZONE'] = '';

    /* формируем массив уведомлений для данного объекта , среди общего списка уведомлений для всех обьектов */
    if(is_array($resultNotifications->events) && !empty($resultNotifications->events)){
        $objId = $obj['id'];

        foreach ($resultNotifications->events as $event){
            if(isset($event->d->unfu) && in_array($objId,$event->d->unfu[1]->un)){


                /* список уведомлений сработавших для данного обьекта по названиям */
                $arNotification[$event->d->unfu[1]->n] = array(
                    'EVENT'=>$event->d->unfu[1]->n,
                    'TIMESTAMP' => date('Y-m-d H:i',$resultNotifications->tm),
                    'MESS_NUMBER' => $event->d->unfu[1]->ac,
                    'ID' => $event->d->unfu[1]->id
                );

                //$wialon_api->getNotificationData($event->d->unfu[1]->id);

                /* подробная информация о уведомлении */
                if(!isset($arDataNotif[$event->d->unfu[1]->id]) || empty($arDataNotif[$event->d->unfu[1]->id])){
                    //$arDataNotif[$event->d->unfu[1]->id] = $wialon_api->getNotificationData($event->d->unfu[1]->id);
                    $arNotification[$event->d->unfu[1]->n]['DATA'] = array_shift(json_decode(json_encode($wialon_api->getNotificationData($event->d->unfu[1]->id)),1));

                }
            }
        }

        //echo '<pre>'.print_r( $resultNotifications->events ,true).'</pre>';
        if(!empty($arNotification)){

            foreach ($arNotification as $notifName => $arNotif){
                
                if($arNotif['DATA']['trg']['p']['type'] == 0){ //событие на срабатывание в геозоне

                    $ar_ids_geozones = explode(',',$arNotif['DATA']['trg']['p']['geozone_ids']);
                    foreach ($ar_ids_geozones as $key => $id) {
                        $arResultFull['IN_GEOZONE'] .= "{$arGeozones[$id]['n']} | \r\n";
                    }
                    
                    //$arResultFull['IN_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} {$arNotif['TIMESTAMP']}, notification name - {$notifName} | \r\n";
                }elseif ($arNotif['DATA']['trg']['p']['type'] == 1){ //событие на срабатывание вне геозоны
                    //$arResultFull['OUT_GEOZONE'] .= "{$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} | \r\n";
                    $ar_ids_geozones = explode(',',$arNotif['DATA']['trg']['p']['geozone_ids']);
                    foreach ($ar_ids_geozones as $key => $id) {
                        $arResultFull['OUT_GEOZONE'] .= "{$arGeozones[$id]['n']} | \r\n";
                    }

                    //$arResultFull['OUT_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} {$arNotif['TIMESTAMP']}, notification name - {$notifName} | \r\n";
                }
            }

            $arAddFields['NOTIFICATION_DATA'] = json_encode($arNotification);
        }
    }



    /* проверка существует ли объект, если да, то апдейтим, если нет, то создаем новую запись */
    $resQuery = $wialon_api->getTableInfo('\Newsite\Wialon\ObjectsTable',array('OBJECT_ID' => $obj['id']));
    if(!empty($arAddFields)){
        if(empty($resQuery)){
            //$resAdd = $wialon_api->addTableLine('\Newsite\Wialon\ObjectsTable',$arAddFields);
        }else{
            $id = key($resQuery);
            //$resUpd = $wialon_api->updateTableLine('\Newsite\Wialon\ObjectsTable',$id,$arAddFields);
        }
    }

    /* заполняем результирующую табличку */
    if(count($arResultFull)>0){
        unset($arResultFull['CUSTOM_FIELDS'],$arResultFull['Z'],$arResultFull['COURSE']);

        $resQuerySummaryTable = $wialon_api->getTableInfo('\Newsite\Wialon\SummaryTable',array('OBJECT_ID' => $obj['id']));

        if(empty($resQuerySummaryTable)){
            $resAdd = $wialon_api->addTableLine('\Newsite\Wialon\SummaryTable',$arResultFull);
        }else{
            $id = key($resQuerySummaryTable);
            $resUpd = $wialon_api->updateTableLine('\Newsite\Wialon\SummaryTable',$id,$arResultFull);    
            //echo '<pre>'.print_r( $arResultFull ,true).'</pre>';
            //print_r($resUpd);
            //die();       
        }
    }

    $arObjId[$obj['id']] = $obj['id'];

	$counter++;
	$exec[$counter] = microtime(true) - $time;
	//echo '<pre>'.print_r( "exec #{$counter} {$exec[$counter]}" ,true).'</pre>';
	//sleep(5);

}


foreach ($arFullTablaObjects as $key => $arObj){
    if(!in_array($arObj['OBJECT_ID'],$arObjId)){
        $resDel = $wialon_api->deleteTableLine('\Newsite\Wialon\SummaryTable',$arObj['ID']);
    }
}

foreach ($arSimpleTablaObjects as $key => $arObj){
    if(!in_array($arObj['OBJECT_ID'],$arObjId)){
        $resDel = $wialon_api->deleteTableLine('\Newsite\Wialon\ObjectsTable',$arObj['ID']);
    }
}

$exectime = time() - $time;
//fwrite(fopen($file, "a"), " - end: ".date('Y-m-d H:i:s',time()) . " - exec time: {$exectime}" . "\n");
