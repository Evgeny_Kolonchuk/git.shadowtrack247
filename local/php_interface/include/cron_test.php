<?php
$_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("newsite.wialon")) {
	return false;
}

$time = microtime(true);

$file = $_SERVER['DOCUMENT_ROOT'] . '/_log.txt';
//fwrite(fopen($file, "a"), "Start ".date('Y-m-d H:i:s',time()));


$wialon_api = new WialonData();
$wialon_api->sessIdUpdate();
$exec0 = microtime(true) - $time;
echo '<pre>'.print_r( "exec #0 {$exec0}" ,true).'</pre>';

$objList = $wialon_api->getObjectsList();
if(empty($objList)){
	return ;
}

/* получаем уведомления */
$resultNotifications = $wialon_api->getEvents();

/* получаем список геозон */
$arGeozones = $wialon_api->getGeozones();

$exec1 = microtime(true) - $time;
echo '<pre>'.print_r( "exec #1 {$exec1}" ,true).'</pre>';
/* подписываем сессию на объекты и передаем в нее события */


foreach ($objList as $key => $arObj){

	$arObj['id'];
	$exec[$arObj['id']] = microtime(true) - $time;
	echo '<pre>'.print_r( "exec #{$arObj['id']} {$exec[$arObj['id']]}" ,true).'</pre>';

	$arAddFields = array(
		'OBJECT_ID' => $arObj['id'],
		'OBJECT_NAME' => $arObj['nm'],
		'TIMESTAMP' => time()//date('Y-m-d H:i:s',time())
	);

	/* идет сбор данных по кастомным полям */
	if(is_array($arObj['flds']) && count($arObj['flds']) > 0){
		foreach ($arObj['flds'] as $customField){
			$arAddFields[$wialon_api->translitCode($customField['n'],'U')] = $customField['v'];
		}
	}

	/* получение данных по последнему местоположению */
	if(is_array($arObj['lmsg']['pos']) && !empty($arObj['lmsg']['pos'])){
		$arAddFields['Y'] = $arObj['lmsg']['pos']['y'];
		$arAddFields['X'] = $arObj['lmsg']['pos']['x'];
		$arAddFields['Z'] = $arObj['lmsg']['pos']['z'];
		$arAddFields['SPEED'] = $arObj['lmsg']['pos']['s'];
		$arAddFields['COURSE'] = $arObj['lmsg']['pos']['c'];
		$arAddFields['DATE_LAST_MSG'] = date('Y-m-d H:i:s',$arObj['lmsg']['t']);
		$location = $wialon_api->getLocationByCoordinates($arAddFields['X'],$arAddFields['Y'],$wialon_api->getUserId()); // 0,4-0,5 сек
		$position = current($location);
		$arAddFields['POSITION'] = !empty($position) ? $position : '';
	}

	/* получаем значение номера вышки */
	$arAddFields['LBS'] = (int)$arObj['lmsg']['p']['cell_id'];


	/* работаем с сенсорами */
	if(is_array($arObj['sens']) && count($arObj['sens']) > 0){
		$arSensorData = $wialon_api->getSensorsData($arObj['id']); // 0,25 сек
		foreach ($arObj['sens'] as $num => $arSens){

			if($arSensorData[$num] == -348201.3876){
				$arSensorData[$num] = '';
			}

			if($arSens['p'] == 'event'){
				$timestamp_before = false;
				$arSensorData[$num] = $wialon_api->getLastSensorBoolValueByTimeStamp($arObj['id'],$arSens['id'],$timestamp_before); // 1 - 1,35 sec не зависит, передаем 1 час или 10 суток
			}

			$arAddFields[$wialon_api->translitCode($arSens['n'],'U')] = $arSensorData[$num];

		}
	}


	/* из списка тянем данные по текущему объекту */
	$arNotification = array();

	$arAddFields['IN_GEOZONE'] = '';
	$arAddFields['OUT_GEOZONE'] = '';


	/* формируем массив уведомлений для данного объекта , среди общего списка уведомлений для всех обьектов */
	if(is_array($resultNotifications->events) && !empty($resultNotifications->events)){
		$objId = $arObj['id'];
		foreach ($resultNotifications->events as $event){
			if(isset($event->d->unfu) && in_array($objId,$event->d->unfu[1]->un)){

				/* список уведомлений сработавших для данного обьекта по названиям */
				$arNotification[$event->d->unfu[1]->n] = array(
					'EVENT'=>$event->d->unfu[1]->n,
					'TIMESTAMP' => date('Y-m-d H:i',$resultNotifications->tm),
					'MESS_NUMBER' => $event->d->unfu[1]->ac,
					'ID' => $event->d->unfu[1]->id
				);

				/* подробная информация о уведомлении */
				$arNotification[$event->d->unfu[1]->n]['DATA'] = array_shift(json_decode(json_encode($wialon_api->getNotificationData($event->d->unfu[1]->id)),1));
			}
		}

		if(!empty($arNotification)){
			foreach ($arNotification as $notifName => $arNotif){
				if($arNotif['DATA']['trg']['p']['type'] == 0){ //событие на срабатывание в геозоне
					$arAddFields['IN_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} | \r\n";
				}elseif ($arNotif['DATA']['trg']['p']['type'] == 1){ //событие на срабатывание вне геозоны
					$arAddFields['OUT_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} | \r\n";
				}
			}
		}
	}


	if(count($arAddFields)>0){
		$add = $wialon_api->cleanDataBeforeSave($arAddFields);
		echo '<pre>'.print_r( $add ,true).'</pre>';
		$resQuerySummaryTable = $wialon_api->getTableInfo('\Newsite\Wialon\SummaryTable',array('OBJECT_ID' => $arObj['id']));

		if(empty($resQuerySummaryTable)){
			//$resAdd = $wialon_api->addTableLine('\Newsite\Wialon\SummaryTable',$add);
		}else{
			//$id = key($resQuerySummaryTable);
			//$resUpd = $wialon_api->updateTableLine('\Newsite\Wialon\SummaryTable',$id,$add);
		}
	}
}



$exec2 = microtime(true) - $time;
echo '<pre>'.print_r( "exec #2 {$exec2}" ,true).'</pre>';
