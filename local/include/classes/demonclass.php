<?
require_once ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/newsite.wialon/lib/wialondata.php');

class DaemonClass {

	// Максимальное количество дочерних процессов
	public $maxProcesses = 10;
	// Когда установится в TRUE, демон завершит работу
	protected $stop_server = false;
	// Здесь будем хранить запущенные дочерние процессы
	protected $currentJobs = array();
	protected $selfPid = 0;

	public $wialon_api = false;
	public $elementsForOneFork = 10;
	public $sleep = 1;
	public $timeFrame = 10;
	public $usleepMilisec = 500000;

	public function __construct() {
		echo "Constructed daemon controller" . PHP_EOL;
		$this->wialon_api = new WialonData();
	}

	/**
	 * Проверка на существование процесса
	 * @param type $pid_file
	 * @return boolean
	 */
	static function isDaemonActive($pid_file) {
		if (is_file($pid_file)) {
			$pid = file_get_contents($pid_file);
			//проверяем на наличие процесса
			if (posix_kill($pid, 0)) {
				error_log('process exits ' . date("d.m.Y H:i:s") . $pid);
				//демон уже запущен
				return true;
			}
			else {
				//pid-файл есть, но процесса нет
				if (!unlink($pid_file)) {
					//не могу уничтожить pid-файл. ошибка
					error_log('can not delete pid file ' . date("d.m.Y H:i:s"));
					exit(-1);
				}
			}
		}
		return false;
	}

	/**
	 *
	 */
	public function run() {

		// Пока $stop_server не установится в TRUE, гоняем бесконечный цикл
		while (!$this->stop_server) {


			//устанавливаю интервал запруска между дочерними процессами
			if ((count($this->currentJobs) <= $this->maxProcesses)){
				$this->launchJob();
			}

			// Если уже запущено максимальное количество дочерних процессов, ждем их завершения
			if (count($this->currentJobs) >= $this->maxProcesses) {
				while(count($this->currentJobs) >= $this->maxProcesses) {
					foreach($this->currentJobs as $pid => $result) {
						$res = pcntl_waitpid($pid, $status, WNOHANG);
						if($res == -1 || $res > 0){
							echo "killed process ({$pid}) " . PHP_EOL;
							unset($this->currentJobs[$pid]);
						}
					}
					sleep($this->sleep);
				}
			}


			//ожидаем 5 секунд до следующего вызова
			if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/StopOptimizatorDaemon")) {
				unlink($_SERVER["DOCUMENT_ROOT"] . "/StopOptimizatorDaemon");
				echo "Stop Daemond " . date("d.m.Y H:i:s") . PHP_EOL;
				$this->stop_server = true;
				break;
			}

			usleep($this->usleepMilisec);
		}
	}

	function updateTables(){

		$arObjects = array();
		$arObjects = $this->wialon_api->getTableInfo('\Newsite\Wialon\ObjectsTable',array('!BUSY'=>false));

		if(!empty($arObjects)) {
			/* блок, когда таблица заполнена */
			//echo print_r($arObjects, true) . PHP_EOL;

			foreach ($arObjects as $id => $arObj) {
				$add = array(
					'OBJECT_ID' => $arObj['OBJECT_ID'],
					'OBJECT_NAME' => $arObj['OBJECT_NAME'],
					'BUSY' => '',
				);
				$this->wialon_api->updateTableLine('\Newsite\Wialon\ObjectsTable', $id, $add);
			}
		}else{

			$this->wialon_api->sessIdUpdate();
			$objList = $this->wialon_api->getObjectsList();

			if(!empty($objList)){
				foreach ($objList as $arrObj){
					$add = array(
						'OBJECT_ID' => $arrObj['id'],
						'OBJECT_NAME' => $arrObj['nm'],
						'TIMESTAMP_START' => time(),
						'TIMESTAMP_FINISH' => '',
						'PROCESS_TIME' => '',
						'BUSY' => '',
					);
					$this->wialon_api->addTableLine('\Newsite\Wialon\ObjectsTable',$add);
				}
			}
		}
	}

	protected function launchJob() {

		// Создаем дочерний процесс
		// весь код после pcntl_fork() будет выполняться
		// двумя процессами: родительским и дочерним
		$pid = pcntl_fork();
		if ($pid == -1) {
			// Не удалось создать дочерний процесс
			error_log('Could not launch new job, exiting');
			return false;
		}
		elseif ($pid) {
			// Этот код выполнится родительским процессом
			$this->currentJobs[$pid] = true;
		}
		else {
			//этот код уже выполняется потомком
			$this->selfPid = getmypid();
			$this->updateStackElemnts($this->selfPid);

			exit();
		}
		return true;
	}


	public function updateStackElemnts($pid) {

		require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
		@ini_set("memory_limit", "256M");
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("newsite.wialon");

		$arObjects = array();
		$arObjects = $this->wialon_api->getTableInfo('\Newsite\Wialon\ObjectsTable',array('BUSY'=>false),false,$this->elementsForOneFork);

		if(!empty($arObjects)){
			$timestamp_start = microtime(true);
			echo 'take to work' . PHP_EOL;
			echo print_r($arObjects,true) . PHP_EOL;

			$arObjToWork = array();
			foreach ($arObjects as $id => $arObj){
				$add = array(
					'OBJECT_ID'=> $arObj['OBJECT_ID'],
					'OBJECT_NAME'=> $arObj['OBJECT_NAME'],
					'TIMESTAMP_START'=> $timestamp_start,
					'TIMESTAMP_FINISH'=> '',
					'PROCESS_TIME'=> '',
					'BUSY' => $pid,
				);
				$this->wialon_api->updateTableLine('\Newsite\Wialon\ObjectsTable', $id, $add);
				$arObjToWork[$id] = $arObj['OBJECT_ID'];
			}

			$arResult = $this->getData($arObjToWork,$timestamp_start);


		}else{
			$this->updateTables();
		}
	}

	function getData($arObjToWork = array(),$timestamp_from){

		if(empty($arObjToWork)){
			return array();
		}

		$time_before_request = microtime(true);

		$this->wialon_api->sessIdUpdate();
		$objList = $this->wialon_api->getObjectsList($arObjToWork);

		if(empty($objList)){
			return ;
		}

		/* получаем уведомления */
		$resultNotifications = $this->wialon_api->getEvents();

		/* получаем список геозон */
		$arGeozones = $this->wialon_api->getGeozones();

		$arResult = array();

		$time_after_request = microtime(true);

		echo "timeout request elements {$this->elementsForOneFork} " . ($time_after_request - $time_before_request) . PHP_EOL;

		$time_last_upd = false;

		foreach ($objList as $key => $arObj){

			$arAddFields = array(
				'OBJECT_ID' => $arObj['id'],
				'OBJECT_NAME' => $arObj['nm'],
				'TIMESTAMP' => time()//date('Y-m-d H:i:s',time())
			);

			/* идет сбор данных по кастомным полям */
			if(is_array($arObj['flds']) && count($arObj['flds']) > 0){
				foreach ($arObj['flds'] as $customField){
					$arAddFields[$this->wialon_api->translitCode($customField['n'],'U')] = $customField['v'];
				}
			}

			/* получение данных по последнему местоположению */
			if(is_array($arObj['lmsg']['pos']) && !empty($arObj['lmsg']['pos'])){
				$arAddFields['Y'] = $arObj['lmsg']['pos']['y'];
				$arAddFields['X'] = $arObj['lmsg']['pos']['x'];
				$arAddFields['Z'] = $arObj['lmsg']['pos']['z'];
				$arAddFields['SPEED'] = $arObj['lmsg']['pos']['s'];
				$arAddFields['COURSE'] = $arObj['lmsg']['pos']['c'];
				$arAddFields['DATE_LAST_MSG'] = date('Y-m-d H:i:s',$arObj['lmsg']['t']);
				$location = $this->wialon_api->getLocationByCoordinates($arAddFields['X'],$arAddFields['Y'],$this->wialon_api->getUserId()); // 0,4-0,5 сек
				$position = current($location);
				$arAddFields['POSITION'] = !empty($position) ? $position : '';
			}

			/* получаем значение номера вышки */
			$arAddFields['LBS'] = (int)$arObj['lmsg']['p']['cell_id'];


			/* работаем с сенсорами */
			if(is_array($arObj['sens']) && count($arObj['sens']) > 0){
				$arSensorData = $this->wialon_api->getSensorsData($arObj['id']); // 0,25 сек
				foreach ($arObj['sens'] as $num => $arSens){

					if($arSensorData[$num] == -348201.3876){
						$arSensorData[$num] = '';
					}

					if($arSens['p'] == 'event'){
						$timestamp_before = false;
						$arSensorData[$num] = $this->wialon_api->getLastSensorBoolValueByTimeStamp($arObj['id'],$arSens['id'],$timestamp_before); // 1 - 1,35 sec не зависит, передаем 1 час или 10 суток
					}

					$arAddFields[$this->wialon_api->translitCode($arSens['n'],'U')] = $arSensorData[$num];

				}
			}


			/* из списка тянем данные по текущему объекту */
			$arNotification = array();

			$arAddFields['IN_GEOZONE'] = '';
			$arAddFields['OUT_GEOZONE'] = '';


			/* формируем массив уведомлений для данного объекта , среди общего списка уведомлений для всех обьектов */
			if(is_array($resultNotifications->events) && !empty($resultNotifications->events)){
				$objId = $arObj['id'];
				foreach ($resultNotifications->events as $event){
					if(isset($event->d->unfu) && in_array($objId,$event->d->unfu[1]->un)){

						/* список уведомлений сработавших для данного обьекта по названиям */
						$arNotification[$event->d->unfu[1]->n] = array(
							'EVENT'=>$event->d->unfu[1]->n,
							'TIMESTAMP' => date('Y-m-d H:i',$resultNotifications->tm),
							'MESS_NUMBER' => $event->d->unfu[1]->ac,
							'ID' => $event->d->unfu[1]->id
						);

						/* подробная информация о уведомлении */
						$arNotification[$event->d->unfu[1]->n]['DATA'] = array_shift(json_decode(json_encode($this->wialon_api->getNotificationData($event->d->unfu[1]->id)),1));
					}
				}

				if(!empty($arNotification)){
					foreach ($arNotification as $notifName => $arNotif){
						if($arNotif['DATA']['trg']['p']['type'] == 0){ //событие на срабатывание в геозоне
							$arAddFields['IN_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} | \r\n";
						}elseif ($arNotif['DATA']['trg']['p']['type'] == 1){ //событие на срабатывание вне геозоны
							$arAddFields['OUT_GEOZONE'] .= "geozone {$arGeozones[$arNotif['DATA']['trg']['p']['geozone_id']]['n']} | \r\n";
						}
					}
				}
			}


			if( (int)$arAddFields['OBJECT_ID'] > 0 ){
				//echo print_r( $add ,true).PHP_EOL;
				$arResult[$arAddFields['OBJECT_ID']] = $this->wialon_api->cleanDataBeforeSave($arAddFields);
				$resQuerySummaryTable = false;
				//$resQuerySummaryTable = $this->wialon_api->getTableInfo('\Newsite\Wialon\SummaryTable',array('OBJECT_ID' => $arObj['id']));

				/* запись отработанного объекта в таблицу с которой идет чтение */
				$cur_time = microtime(true);

				if(!$time_last_upd){
					$time_last_upd = $cur_time - $time_after_request;
				} ;

				$id = array_search($arAddFields['OBJECT_ID'],$arObjToWork);
				$add = array(
					'OBJECT_ID'=> $arAddFields['OBJECT_ID'],
					'OBJECT_NAME'=> $arAddFields['OBJECT_NAME'],
					'TIMESTAMP_FINISH'=> $cur_time,
					//'PROCESS_TIME'=> $time_last_upd
					'PROCESS_TIME'=> $cur_time - $timestamp_from
				);

				$this->wialon_api->updateTableLine('\Newsite\Wialon\ObjectsTable', $id, $add);


				if(empty($resQuerySummaryTable)){
					//$resAdd = $this->wialon_api->addTableLine('\Newsite\Wialon\SummaryTable',$add);
				}else{
					//$id = key($resQuerySummaryTable);
					//$resUpd = $this->wialon_api->updateTableLine('\Newsite\Wialon\SummaryTable',$id,$add);
				}
			}
		}
		return $arResult;
	}

}
